
/*
Napisite klasu ZooAnimal koja opisuje zivotinje u zooloskom vrtu. Podaci o zivotinji su:
vrsta,
ime,
godina rodenja,
broj kaveza,
broj dnevnih obroka hrane,
ocekivani zivotnivijek i
niz podataka o masi zivotinje koji se biljezi svaku godinu (podatak o masi je iznos mase i godina vaganja).
Niz podataka o masi treba biti dinamicki alociran u konstruktoru, a alocirana velicina niza treba biti dovoljna za zivot dug dvostruko od ocekivanog zivotnog vijeka.
Napisite konstruktore i sljedece member funkcije:
•konstruktor koji ima sest parametara: vrstu, ime, godinu rodenja, broj kaveza, broj obroka i ocekivani zivotni vijek,
•destruktor,
•copykonstruktor,
•funkciju za promjenu broja obroka (smanjenje ili uvecanje za 1),
•funkciju koja dodaje podatke o masi za odredenu godinu (provjeriti da li vec postoje podaci o toj godini i ako postoje i nisu za tekucu godinu, ne dozvoliti promjenu),
•funkciju koja detektira da li se zivotinja udebljala ili je smrsavila vise od 10% u zadnjih godinu dana (tekucu godinu odredite pomocu funkcija iz ctime),
•funkciju koja ispisuje podatke o objektu.
Funkcije koje ne mijenjaju podatke oznacite const.
Razmislite da li je potreban default konstruktor i ako je, napisite ga.
U main funkciji napunite vektor zivotinjama, te napisite funkciju koja za pojedinu zivotinju mijenja podatke o broju obroka ukoliko je zivotinja podhranjena ili predebela.
Ispisite vektor prije i poslije promjena.
U konstruktorima i destruktorima ispisite poruke i pokusajte zakljuciti u kojem trenutkuse pozivaju.
Napomena: u copykonstruktoru se, osim same vrijednosti pointera, kopiraju i vrijednosti niza (deep copy).
Napomena 2: odvojite implementaciju i sucelje klase u razlicite datoteke.
*/

#pragma once

#include <iostream>
#include <string>
#include <vector>

using namespace std;


struct Masa {

	int m_godinaVaganja;
	int m_masaZivotinje;

};


class ZooAnimal {

private:

	string m_vrsta;
	string m_ime;
	int m_godinaRodjenja;
	int m_brojKaveza;
	int m_brojDnevnihObroka;
	int m_ocekivaniZivotniVijek;
	Masa* m_masaPodaci;

public:

	//ZooAnimal();
	ZooAnimal(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek);
	~ZooAnimal();
	ZooAnimal(const ZooAnimal& copyZooAnimal);
	void PromjenaDnevnihObroka(bool promjena);
	void DodajPodatkeOMasi(int godinaVaganja, int masaZivotinje);
	void IspisiObjekt();
	bool ProvjeraMase();




};
