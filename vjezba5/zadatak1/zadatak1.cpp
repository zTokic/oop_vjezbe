/*
Napisite klasu ZooAnimal koja opisuje zivotinje u zooloskom vrtu. Podaci o zivotinji su: 
vrsta, 
ime, 
godina rodenja, 
broj kaveza, 
broj dnevnih obroka hrane, 
ocekivani zivotnivijek i 
niz podataka o masi zivotinje koji se biljezi svaku godinu (podatak o masi je iznos mase i godina vaganja).
Niz podataka o masi treba biti dinamicki alociran u konstruktoru, a alocirana velicina niza treba biti dovoljna za zivot dug dvostruko od ocekivanog zivotnog vijeka.  
Napisite konstruktore i sljedece member funkcije:
•konstruktor koji ima sest parametara: vrstu, ime, godinu rodenja, broj kaveza, broj obroka i ocekivani zivotni vijek,
•destruktor,
•copykonstruktor,
•funkciju za promjenu broja obroka (smanjenje ili uvecanje za 1),
•funkciju koja dodaje podatke o masi za odredenu godinu (provjeriti da li vec postoje podaci o toj godini i ako postoje i nisu za tekucu godinu, ne dozvoliti promjenu),
•funkciju koja detektira da li se zivotinja udebljala ili je smrsavila vise od 10% uzadnjih godinu dana (tekucu godinu odredite pomocu funkcija iz ctime),
•funkciju koja ispisuje podatke o objektu.
Funkcije koje ne mijenjaju podatke oznacite const.
Razmislite da li je potreban default konstruktor i ako je, napisite ga.
U main funkciji napunite vektor zivotinjama, te napisite funkciju koja za pojedinu zivotinju mijenja podatke o broju obroka ukoliko je zivotinja podhranjena ili predebela. 
Ispisite vektor prije i poslije promjena. 
U konstruktorima i destruktorima ispisite poruke i pokusajte zakljuciti u kojem trenutkuse pozivaju.
Napomena: u copykonstruktoru se, osim same vrijednosti pointera, kopiraju i vrijednosti niza (deep copy).
Napomena 2: odvojite implementaciju i sucelje klase u razlicite datoteke.
*/

#include <iostream>
#include "ZooAnimal.h"


using namespace std;

int main() {

	vector<ZooAnimal> animals;

	//no defalut constructor exists for class ZooAnimal
	//ZooAnimal medvid;	
	ZooAnimal zivotinja1("medvjed", "ivan", 2005, 0, 4, 30);
	ZooAnimal zivotinja2("jelen", "ivan", 2005, 0, 4, 30);
	//ZooAnimal zivotinja3("deva", "ivan", 2005, 0, 4, 30);
	//animals.push_back(zivotinja1);
	//animals.push_back(zivotinja2);
	//animals.push_back(zivotinja3);

	zivotinja1.DodajPodatkeOMasi(2018, 15);
	zivotinja1.DodajPodatkeOMasi(2019, 33);
	zivotinja1.IspisiObjekt();

	bool dodajSmanji = zivotinja1.ProvjeraMase();

	zivotinja1.PromjenaDnevnihObroka(dodajSmanji);
	zivotinja1.IspisiObjekt();

	animals.push_back(zivotinja2);
	for (int i = 0; i < animals.size(); i++) {
		animals[0].IspisiObjekt();
	}


	cin.get();


}