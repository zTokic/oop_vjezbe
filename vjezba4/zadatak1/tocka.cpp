#include <iostream>
#include <random>
#include <vector>
#include <math.h>
#include "pch.h"
#include "tocka.h"

using namespace std;

void tocka::postaviKoo(double x, double y, double z) {

	this->x = x;
	this->y = y;
	this->z = z;

}

int getRandomIDX(int const a = 0, int const b = 10) {

	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(a, b);
	return dis(gen);
}

void tocka::randomKoo() {

	int a = 0;
	int b = 10;
	std::mt19937 rng(10);
	std::uniform_int_distribution<int> gen(a, b);
	this->x = getRandomIDX(a, b);
	this->y = getRandomIDX(a, b);
	this->z = getRandomIDX(a, b);
}

void tocka::printajKoo() {

	cout << "X: " << this->x << endl;
	cout << "Y: " << this->y << endl;
	cout << "Z: " << this->z << endl;

}

double tocka::dist2D(tocka koo) {

	double kat1 = this->x - koo.x;
	double kat2 = this->y - koo.y;

	double hipotenuza = sqrt((kat1*kat1) + (kat2*kat2));

	return hipotenuza;

}

double tocka::dist3D(tocka koo) {

	double kat1 = this->x - koo.x;
	double kat2 = this->y - koo.y;
	double visina = this->z - koo.z;

	double hipotenuza = sqrt((kat1*kat1) + (kat2*kat2));
	double udaljenost = sqrt((hipotenuza*hipotenuza) + (visina*visina));
	return udaljenost;

}

double tocka::GetX(tocka koo) {

	return koo.x;

}

double tocka::GetY(tocka koo) {

	return koo.y;

}

double tocka::GetZ(tocka koo) {

	return koo.z;

}