/*
Napisati klasu za poziciju tocke u prostoru koja ima tri double podatka: duzinu, sirinu i visinu. Klasa ima sljedece funkcije:
� Funkciju za postavljanje vrijednosti clanova (treba imati pretpostavljene vrijednosti 0).
� Funkciju za postavljanje pseudorandom vrijednosti clanova (granice intervala su parametri funkcije).
� Funkciju za dohvacanje vrijednosti clanova.
� Funkciju koja racuna udaljenost do druge pozicije u 2D ravnini.
� Funkciju koja racuna udaljenost do druge pozicije u 3D prostoru.
U main funkciji postavite vrijednost za dvije tocke u prostoru i izracunajte obe udaljenosti.
*/

#include "pch.h"
#include "tocka.h"
#include <iostream>

using namespace std;

int main() {

	tocka poz1, poz2;
	poz1.postaviKoo(5, 3, 4);
	poz1.printajKoo();
	poz2.randomKoo();
	poz2.printajKoo();
	cout << "2D udaljenost je: " << poz1.dist2D(poz2) << endl;
	cout << "3D udaljenost je: " << poz1.dist3D(poz2) << endl;
	cout << "poz1 X: " << poz1.GetX(poz1) << endl;
	cout << "poz2 X: " << poz2.GetX(poz2) << endl;
	
	}


