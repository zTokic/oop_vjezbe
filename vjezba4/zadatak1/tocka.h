#ifndef TOCKA_H
#define TOCKA_H

#include <iostream>
#include <random>
#include <vector>
#include <math.h>

using namespace std;

class tocka {

private:

	double x = 0;
	double y = 0;
	double z = 0;

public:

	void postaviKoo(double x, double y, double z);
	void randomKoo();
	void printajKoo();
	double dist2D(tocka koo);
	double dist3D(tocka koo);
	double GetX(tocka koo);
	double GetY(tocka koo);
	double GetZ(tocka koo);

};

#endif // TOCKA_H

