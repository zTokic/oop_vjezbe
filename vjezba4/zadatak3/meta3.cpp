

#include "pch.h"
#include <iostream>
#include <random>
#include <vector>
#include <math.h>
#include <string>
#include "tocka3.h"
#include "oruzje3.h"
#include "meta3.h"


using namespace std;


void meta::PostaviMetu(double sirina, double visina) {

	m_doliLivo.randomKoo();
	m_sirina = sirina;
	m_visina = visina;
	cout << "DOLI LIVO KOO:" << endl;
	m_doliLivo.printajKoo();
	cout << "SIRINA: " << m_sirina << endl;
	cout << "VISINA: " << m_visina << endl;

}

void meta::ProvjeraPogodtka(tocka RandomHitac) {
	
	double hitacX = RandomHitac.GetX(RandomHitac);
	cout << "HITAC X: " << hitacX << endl;
	double hitacY = RandomHitac.GetY(RandomHitac);
	cout << "HITAC Y: " << hitacY << endl;

	double doliLivoX = m_doliLivo.GetX(m_doliLivo);
	cout << "DOLI LIVO X: " << doliLivoX << endl;
	double doliLivoY = m_doliLivo.GetY(m_doliLivo);
	cout << "DOLI LIVO Y: " << doliLivoY << endl;
	double doliDesnoX = doliLivoX + m_sirina;
	cout << "DOLI DESNO X: " << doliDesnoX << endl;
	double goriLivoY = doliLivoY + m_visina;
	cout << "GORI LIVO Y: " << goriLivoY << endl; 

	if ((hitacX <= doliDesnoX && hitacX >= doliLivoX) && (hitacY >= doliLivoY && hitacY <= goriLivoY)) {
		m_stanje = "Pogodjena";
		cout << "Pogodjena" << endl;
	}
	else
		cout << m_stanje << endl;

}

