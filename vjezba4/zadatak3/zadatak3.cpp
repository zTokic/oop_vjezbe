/*
Napisati klasu koja predstavlja metu. 
Meta je oblika pravokutnika i zadana je donjom lijevom tockom, sirinom i visinom. 
Ima dva stanja: pogodena i nepogodena.
*/

#include "pch.h"
#include "tocka3.h"
#include "meta3.h"
#include "oruzje3.h"
#include <iostream>

using namespace std;

int main() {

	meta meta1;
	meta1.PostaviMetu(10, 10);
	tocka hitac;
	hitac.randomKoo();
	meta1.ProvjeraPogodtka(hitac);

}


