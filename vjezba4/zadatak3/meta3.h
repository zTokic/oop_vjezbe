#pragma once
#include <iostream>

class meta {

	tocka m_doliLivo;
	double m_sirina;
	double m_visina;
	string m_stanje = "Nepogodjena";

public:

	void PostaviMetu(double sirina, double visina);
	void ProvjeraPogodtka(tocka RandomHitac);

};
