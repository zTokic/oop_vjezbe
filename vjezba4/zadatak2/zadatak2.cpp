/*
Napisati klasu koja predstavlja oruzje. 
Oruzje ima svoj polozaj u prostoru (jedna tocka u prostoru), broj metaka koji stanu u jedno punjenje i trenutni broj metaka u punjenju. 
Moze pucati (shoot) i ponovo se puniti (reload).
*/

#include "pch.h"
#include "oruzje2.h"
#include "tocka2.h"
#include <iostream>

using namespace std;

int main() {

	oruzje pistolj;

	pistolj.Set(6);
	pistolj.Shoot();
	pistolj.Shoot();
	pistolj.Shoot();
	pistolj.Reload();
	pistolj.Shoot();
	pistolj.Shoot();



}


