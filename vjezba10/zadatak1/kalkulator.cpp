#include<iostream>
#include<string>
#include<fstream>
#include"kalkulator.h"

using namespace std;

double UnosBroja() {

	double unos;
	cin >> unos;

	if (cin.fail()) //provjera datatype krivo 1 dobro 0
		throw ProvjeraUnosaBroja();
	
	return unos;
}

double Rezultat(double prvi, double drugi, char znak) {

	if (drugi == 0 && znak == '/')
		throw ProvjeraDijeljivosti();
	
	switch (znak) {
	case '+':
		return prvi + drugi;
	case '-':
		return prvi - drugi;
	case '*':
		return prvi * drugi;
	case '/':
		return prvi / drugi;
	default:
		break;
	}
}

char UnosOperatora() {

	char unos;
	cin >> unos;
	if (!(unos == '+' || unos == '-' || unos == '*' || unos == '/')) {
		throw ProvjeraUnosaOperatora();
	}
	return unos;
}

void ZapisGresaka(string greske) {
	
	ofstream fout ("errors.log", ios_base::out | ios_base::app);
	fout << greske << endl;
	fout.close();

}

void ZapisRezultata(double prvi, double drugi, char znak, double rezultati) {

	ofstream fout("errors.log", ios_base::out | ios_base::app);
	fout << ">>>  " << prvi << " " << znak << " " << drugi << " = " << rezultati << endl;
	fout.close();

}
