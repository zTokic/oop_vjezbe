#pragma once


#include<iostream>
#include<string>

using namespace std;



class BazaGresaka {
public:
	virtual string Greska() = 0;
};

class ProvjeraUnosaBroja : public BazaGresaka {
public:
	string Greska() override {
		return "Nije unesen broj";
	}
};

class ProvjeraUnosaOperatora : public BazaGresaka {
public:
	string Greska() override {
		return "Operator ne postoji";
	}
};

class ProvjeraDijeljivosti : public BazaGresaka {
public:
	string Greska() override {
		//cout << "Nije moguce dijeliti sa nulom (0)" << endl;
		return "Nije moguce dijeliti sa nulom (0)";
	}
};

double UnosBroja();
double Rezultat(double prvi, double drugi, char znak);
char UnosOperatora();
void ZapisGresaka(string greske);
void ZapisRezultata(double prvi, double drugi, char znak, double rezultati);