/*

Napisite program kojim se unose dva operanda (cijeli brojevi) i operator. 
Pri tome obavezno koristite sljedece funkcije: 
� funkciju za unos jednog cijelog broja - baca iznimku ako nije unesen broj */
//� funkciju za unos operatora - baca iznimku ako operator nije jedan od podrzanih (+-*/) 
/*
� funkciju koja racuna rezultat operacije - baca iznimku u slucaju problema(npr.dijeljenje s nulom)
Sve iznimke izvedite iz iste bazne iznimke.
U glavnom programu vrtite beskonacnu petlju dok se operacija uspjesno racuna i rezultat sprema u vektor.
U slucaju iznimke otvorite datoteku sa greskama(errors.log) i u nju dodajte novi red sa opisom pogreske.
Uputstva: 
� za zapis u datoteku koristite standardnu klasu 
ofstream - koristite konstruktor sa nazivom datoteke i modom otvaranja(ios base::out | ios base::app)
ofstream fout("errors.log", ios_base::out | ios_base::app); 
� standarni stream za unos(cin) ima metodu fail() za provjeru pogreske std::cin >> n;
if (std::cin.fail())...

*/


#include<iostream>
#include<vector>
#include"kalkulator.h"

using namespace std;

int main() {

	vector<double>result;
	try {
		while (true) {
			cout << "KALKULATOR" << endl;
			cout << "Unesi prvi broj: ";
			double prvi = UnosBroja();
			cout << endl;
			cout << "Unesi operator: ";
			char znak = UnosOperatora();
			cout << endl;
			cout << "Unesi drugi broj: ";
			double drugi = UnosBroja();
			cout << endl;
			double rezultat = Rezultat(prvi, drugi, znak);
			cout << "Rezultat operacije je: " << rezultat << endl;
			ZapisRezultata(prvi, drugi, znak, rezultat);
			result.push_back(rezultat);
			cin.get();
		}
	}
	catch (BazaGresaka& greska) {
		cout << "ERROR: " << greska.Greska() << endl;
		ZapisGresaka(greska.Greska());
		cin.get();
	}
	catch (...) {
		cout << "Nepoznata greska";
		ZapisGresaka("Nepoznata greska");
		cin.get();
	}


	cin.get();


}

