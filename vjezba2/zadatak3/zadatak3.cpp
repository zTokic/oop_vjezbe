/*

Definirati strukturu koja simulira rad vektora. Struktura se sastoji od niza int elemenata, logicke i fizicke velicine niza.  
Fizicka velicina je inicijalno init, a kada se ta velicina napuni vrijednostima, alocira se duplo.
Napisati implementacije za funkcije 
vector_new, vector_delete, vector_pushback, vector_popback, vector_front, vector_back i vector_size.
*/

#include "pch.h"
#include <iostream>

using namespace std;


struct MyVector {

	int *arr;
	int size = 0;//fizicka
	int len = 0;//logicka

	void vector_new(int sizeN) {

		size = sizeN;
		arr = new int[size];
		
	}

	void vector_delete() {

		delete[] arr;
		arr = NULL;

	}
	
	void vector_pushback(int unos) {
		
		if (len == size) {

			int *temp = new int[size];//alocira temp niz
			copy(arr, arr + len, temp);//kopira u temp niz
			arr = new int[(size * 2)];//alocira duplo
			copy(temp, temp + len, arr);//kopira tamo
			delete[] temp;
			temp = NULL;
			size *= 2;
		}

		arr[len] = unos;
		len++;


	}

	void vector_print() {

		cout << "printanje...\n";
		for (int i = 0; i < len; i++) {
			cout << arr[i] << endl;
		}
	
	}

	void vector_popback() {

		len -= 1;

	}

	int vector_front() {

		//cout << arr[0];
		return arr[0];

	}

	int vector_back() {

		//cout << arr[len-1];
		return arr[len - 1];
	}

	int vector_size() {

		return len;

	}

};


int main(){

	MyVector vektor1;
	vektor1.vector_new(3);

	//vektor1.vector_delete();
	
	cout << "velicina je: " << vektor1.size << endl;
	cout << "duzina je: " << vektor1.len << endl;

	cout << "ide pushback\n";
	for (int i = 50; i < 55; i++) {
		vektor1.vector_pushback(i);
	}
	cout << "gotov pushback\n";
	cout << "velicina je: " << vektor1.size << endl;
	cout << "duzina je: " << vektor1.len << endl;

	vektor1.vector_print();
	cout << "ide popback\n";
	vektor1.vector_popback();
	cout << "gotov popback\n";

	vektor1.vector_print();
	cout << "velicina je: " << vektor1.size << endl;
	cout << "duzina je: " << vektor1.len << endl;
	cout << "zadnji clan je: " << vektor1.vector_back() << endl;
	cout << "prvi clan je: " << vektor1.vector_front() << endl;

	
}


