/*
Napisite funkciju koja odvaja parne i neparne brojeve u nizu tako da se prvo pojavljuju parni brojevi, a zatim neparni brojevi.  
Memoriju za niz alocirati dinamicki. Primjer ulaza i izlaza: 
ulaz: 7 2 4 9 10 11 13 27
izlaz: 10 2 4 9 7 11 13 27
*/

#include "pch.h"
#include <iostream>

void swap(int *xp, int *yp){
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void bubbleSort(int arr[], int n){
	int i, j;
	bool swapped;
	for (i = 0; i < n - 1; i++){
		swapped = false;
		for (j = 0; j < n - i - 1; j++){
			if (arr[j] > arr[j + 1]){
				swap(&arr[j], &arr[j + 1]);
				swapped = true;
			}
		}

		if (swapped == false)
			break;
	}
}


void sortiraj(int *arr) {

	int *temp = new int[8];
	int i, j=0;
	for (i = 0; i < 8; i++) {
		if (arr[i] % 2 == 0) {
			temp[j] = arr[i];
			j++;
		}
	}

	for (i = 0; i < 8; i++) {
		if (arr[i] % 2 != 0) {
			temp[j] = arr[i];
			j++;
		}
	}

	std::cout << "\nSortirani niz je: \n";
	for (i = 0; i < 8; i++) {
		std::cout << temp[i] << " ";
	}

	delete[] temp;
	temp = NULL;
}


int main(){

	int i;
	int *arr = new int [8];

	std::cout << "Unesi 8 clanova niza:\n";
	for (int i = 0; i < 8; i++) {
		std::cin >> arr[i];
	}

	std::cout << "Uneseni niz je: \n";
	for (i = 0; i < 8; i++) {
		std::cout << arr[i] << " ";
	}
	bubbleSort(arr, 8);
	sortiraj(arr);

	delete[] arr;
	arr = NULL;

}

