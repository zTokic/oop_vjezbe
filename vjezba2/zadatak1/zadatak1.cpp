/*
Napisati funkciju koja pronalazi element koji nedostaje u cjelobrojnom rasponu 1-9.
Memoriju za niz alocirati dinamicki, korisnik popunjava niz, a funkcija prepoznaje i vraca modificirani sortirani niz s brojem koji nedostaje. 
U main funkciji ispisati dobiveni niz i osloboditi memoriju.
*/

#include "pch.h"
#include <iostream>



void swap(int *xp, int *yp) {
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void bubbleSort(int arr[], int n) {
	int i, j;
	bool swapped;
	for (i = 0; i < n - 1; i++) {
		swapped = false;
		for (j = 0; j < n - i - 1; j++) {
			if (arr[j] > arr[j + 1]) {
				swap(&arr[j], &arr[j + 1]);
				swapped = true;
			}
		}

		if (swapped == false)
			break;
	}
}

int *sortiraj(int *arr, int n) {

	//pronalazak broja
	int i, j = 0, broj;
	int *sort = new int[n + 1];
	broj = (n + 1)*(n + 2) / 2;
	for (i = 0; i < n; i++) {
		sort[j] = arr[i];
		j++;
		broj -= arr[i];
	}
	sort[j] = broj;
	std::cout << "broj koji fali je " << broj << std::endl;

	return sort;

}

int main(){

	int i, n;
	//std::cout << "Unesi velicinu niza:" << std::endl;
	//std::cin >> n;
	n = 4;
	std::cout << std::endl;
	int *arr;
	arr = new int[n];
	for (i = 0; i < n; i++) {
		std::cout << "Unesi clan niza:" << std::endl;
		std::cin >> arr[i];
	};

	int *p;
	p = sortiraj(arr, n);

	bubbleSort(p, n + 1);
	
	//ispis
	int vel = n + 1;
	for (i = 0; i < vel; i++) {
		std::cout << p[i] << " ";
	}

	delete[] arr;
	arr = NULL;
	delete[] p;
	p = NULL;
}


