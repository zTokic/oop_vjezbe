/*
Koristeci vector napravite implementaciju igre "sibica" gdje korisnik igra protiv racunala.
Pravila ove igre su vrlo jednostavna.
Pred 2 igraca postavi se 21 sibica.
Igraci se izmjenjuju i uklanjaju 1, 2 ili 3 sibice odjednom.
Igrac koji je prisiljen uzeti posljednju sibicu gubi.
Korisnik unosi izbor, dok se za odabir racunala bira slucajnim izborom. 
Igracu se mora dati prednost, tako da racunalo prvo zapocinje igru.
Prije pisanja koda promislite o problemu, 
koji su moguci slucajevi, 
kad je sigurna pobjeda, 
kad je neizbjezan poraz (procitati �Problem sibica - pomoc�).
*/

#include "pch.h"
#include <iostream>
#include <vector>
#include <random>

using std::vector;

int getRandomIDX(int const a = 0, int const b = 100)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(a, b);
	return dis(gen);
}

void sibice(vector<int>vekt) {

	std::mt19937 rng(100);
	std::uniform_int_distribution<int> gen(1, 3);
	
	int cntSibice, korisnik, racunalo;
	int flag = 0;	//racunalo-0, korisnik-1
	std::cout << "VELKAM TO SIBICE\nUNESI BROJEVE 1-3 (ne vise i ne manje)\n\n";
	while (vekt.size() != 0) {

		if (flag == 0 && vekt.size() == 1) {
			std::cout << "Korisnik je pobjednik";
			break;
		}
		else if (flag == 1 && vekt.size() == 1) {
			std::cout << "Racunalo je pobjednik";
			break;
		}
		else if (flag == 1 && vekt.size() == 2) {
			std::cout << "Korisnik je pobjednik";
			break;
		}
		else if (flag == 0 && vekt.size() == 2) {
			std::cout << "Racunalo je pobjednik";
			break;
		}

		//RACUNALO
		cntSibice = getRandomIDX(1, 3);
		std::cout << "Racunalo je izvuklo: " << cntSibice << std::endl;
		
		while (cntSibice) {
			vekt.pop_back();
			cntSibice--;
		}
		std::cout << "Preostalo je " << vekt.size() << " sibica." << std::endl << std::endl;

		flag = 1;

		//KORISNIK
		std::cout << "Korisnik unosi: ";
		std::cin >> cntSibice;

		while (cntSibice) {
			vekt.pop_back();
			cntSibice--;
		}
		std::cout << "Preostalo je " << vekt.size() << " sibica." << std::endl << std::endl;

		flag = 0;
	}
}

int main(){

	vector<int> vekt(21, 1);
	sibice(vekt);

}
