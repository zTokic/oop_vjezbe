/*
Napisati funkcije za unos i ispis vektora. 
Funkcija za unos treba imati sljedece parametre: 
a i b koji predstavljaju granice intervala iz kojeg moraju biti elementi vektora i cija je pretpostavljena vrijednost 0 i 100, 
broj elemenata n sa pretpostavljenom vrijednoscu 5 i bool vrijednost koja odreduje hoce li se elementi upisivati ili ce se generirati pseudorandom funkcijom. 
Funkcija za unos vraca vektor.
*/

#include "pch.h"
#include <iostream>
#include <vector>
#include <random>
#include <ctime>

using std::vector;

vector<int>UnosVektora(bool odabir, int n = 5, int a = 0, int b = 100) {
	
	vector<int> vekt;
	srand(time(NULL));
	int i = 0, unos;

	if (odabir) {
		for (i = 0; i < n; i++) {
			vekt.push_back((rand() % 100) + 0);
		}
	}
	else {
		for (i = 0; i < n; i++) {
			std::cout << "Unesi brojeve od 0-100" << std::endl;
			std::cin >> unos;
			while (unos < a || unos > b){
				std::cout << "Unos pogresan, broj nije u rasponu 1-100. Ponovite unos:" << std::endl;
				std::cin >> unos;
			}
			vekt.push_back(unos);
		}
	}
	return vekt;
}

void PrintVektora(vector<int> vektor)
{
	for (int i = 0; i < vektor.size(); i++) {
		std::cout << vektor.at(i) << ' ';
	}
}

int main() {

	int odabir;
	std::cout << "1-Generiraj vektor\n2-Unesi rucno\n";
	std::cin >> odabir;
	if (odabir == 1)
		odabir = true;
	else
		odabir = false;
	vector<int> vektor = UnosVektora(odabir);
	PrintVektora(vektor);
	return 0;
}
