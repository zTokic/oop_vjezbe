/*
Ucitati string koji predstavlja recenicu. 
Napisati funkciju koja iz stringa izbacuje sve praznine koje se nalaze ispred znakova interpunkcije i dodaje praznine nakon znaka interpunkcije ako nedostaju.
Primjer: Za recenicu �Ja bih ,ako ikako mogu ,ovu recenicu napisala ispravno .�, ispravna recenica glasi : �Ja bih, ako ikako mogu, ovu recenicu napisala ispravno.�.
*/

#include "pch.h"
#include <iostream>
#include <string>

void ispravno(std::string recenica) {

	int duljina = recenica.size();
	//char *nova[duljina];
	char *nova;
	nova = new char[duljina];

	//std::string nova="";
	
	for (int i = 0; i < duljina; i++) {
		if (recenica[i] == ',') {
			nova[i - 1] = ',';
			nova[i] = ' ';
		}
		else if (recenica[i] == '.') {
			nova[i - 1] = '.';
			nova[i] = ' ';
		}
		else if (i == duljina) {
			nova[i] = '\0';
		}
		else {
			nova[i] = recenica[i];
		}
		//nova[duljina] == '\0';
	}
	recenica = nova;
	delete[] nova;
	nova = NULL;
	std::cout << recenica << std::endl;

	/*
	for (int i = 0; i < duljina; i++) {
		if (recenica.at(i) == ',') {
			nova.at(i-1) = ',';
			nova.at(i) = ' ';
		}
		else if (recenica.at(i) == '.') {
			nova.at(i-1) = '.';
			nova.at(i) = ' ';
		}
		else {
			nova.at(i) = recenica.at(i);
		}
	}

	std::cout << nova << std::endl;*/

}

int main(){
	
	std::string recenica = "Ja bih ,ako ikako mogu ,ovu recenicu napisala ispravno .";
	
	//int duljina = recenica.length();
	int velicina = recenica.size();
	
	//std::cout << duljina << std::endl;
	std::cout << velicina << std::endl;

	std::cout << recenica << std::endl;

	ispravno(recenica);
	
}
