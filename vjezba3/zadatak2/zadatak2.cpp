/*
Koristeci funkcije iz prvog zadatka unijeti dva vektora i formirati novi vektor koji se sastoji od elemenata iz prvog vektora koji nisu u drugom vektoru. 
Koristiti binary search funkciju.
Funkcije i njihove prototipe napisati u posebnim datotekama.
*/


#include "pch.h"
#include <iostream>
#include <vector>
#include <random>
#include <ctime>

using std::vector;

int getRandomIDX(int const a = 0, int const b = 100)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(a, b);
	return dis(gen);
}

vector<int>UnosVektora(bool odabir, int n = 5, int a = 0, int b = 100) {

	vector<int> vekt;
	//srand(time(NULL));
	int i = 0, unos;
	std::mt19937 rng(100);
	std::uniform_int_distribution<int> gen(a, b);

	if (odabir) {
		for (i = 0; i < n; i++) {
			//vekt.push_back((rand() % 100) + 0);
			vekt.push_back(getRandomIDX(a,b));
		}
	}
	else {
		for (i = 0; i < n; i++) {
			std::cout << "Unesi brojeve od 0-100" << std::endl;
			std::cin >> unos;
			while (unos < a || unos > b) {
				std::cout << "Unos pogresan, broj nije u rasponu 1-100. Ponovite unos:" << std::endl;
				std::cin >> unos;
			}
			vekt.push_back(unos);
		}
	}
	return vekt;
}

void PrintVektora(vector<int> vektor)
{
	for (int i = 0; i < vektor.size(); i++) {
		std::cout << vektor.at(i) << ' ';
	}
	std::cout << std::endl;
}

vector<int>Spajanje(vector<int>vektor1, vector<int>vektor2) {

	vector<int> spojeni;

	std::sort(vektor1.begin(), vektor1.end());
	std::sort(vektor2.begin(), vektor2.end());

	for (int i = 0; i < vektor1.size(); i++){
		if (!std::binary_search(vektor2.begin(), vektor2.end(), vektor1.at(i))){
			spojeni.push_back(vektor1.at(i));
		}
	}

	return spojeni;

}

int main() {

	int odabir;
	std::cout << "1-Generiraj vektor\n2-Unesi rucno\n";
	std::cin >> odabir;
	if (odabir == 1)
		odabir = true;
	else
		odabir = false;

	vector<int> vektor1 = UnosVektora(odabir);
	PrintVektora(vektor1);

	vector<int> vektor2 = UnosVektora(odabir);
	PrintVektora(vektor2);

	vector<int> spojeni = Spajanje(vektor1, vektor2);
	std::cout << "SPOJENI:\n";
	PrintVektora(spojeni);
	
	return 0;
}
