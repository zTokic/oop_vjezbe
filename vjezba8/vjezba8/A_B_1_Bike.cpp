#include<iostream>
#include<string>
#include<vector>
#include"A_Vehicle.h"
#include"A_B_land_vehicle.h"
#include"A_B_1_Bike.h"

using namespace std;
using namespace oop;

bike::bike() {

	cout << "Constructing bike" << endl;
	totalPassengers = Passengers();

}

bike::~bike() {

	cout << "Destructing bike" << endl;

}

unsigned bike::Passengers() {

	return 1;

}

string bike::Name() {

	return "Bike";

}