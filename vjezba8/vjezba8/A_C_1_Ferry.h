#pragma once

#include<iostream>
#include<vector>
#include<string>
#include"A_C_watercraft.h"


using namespace std;

namespace oop {

	class ferry : public watercraft {

	protected:

		unsigned totalPassengers;

	public:

		unsigned Passengers() override;
		string Name() override;
		ferry(unsigned int totalBike, unsigned int totalCar, unsigned int a_totalPassengers);
		~ferry();

	};


}