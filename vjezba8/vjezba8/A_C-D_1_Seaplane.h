#pragma once

#include<iostream>
#include<vector>
#include<string>
#include"A_C_watercraft.h"
#include"A_D_aircraft.h"



using namespace std;

namespace oop {

	class seaplane : public watercraft, public aircraft {

	protected:

		unsigned totalPassengers;

	public:

		string vehicleType;
		unsigned Passengers() override;
		string Type() override;
		string Name() override;
		seaplane(unsigned int a_totalPassengers);
		~seaplane();

	};


}