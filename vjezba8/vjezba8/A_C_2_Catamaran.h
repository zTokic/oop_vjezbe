#pragma once

#include<iostream>
#include<vector>
#include<string>
#include"A_C_watercraft.h"


using namespace std;

namespace oop {

	class catamaran : public watercraft {

	protected:

		unsigned totalPassengers;

	public:

		unsigned Passengers() override;
		string Name() override;
		catamaran(unsigned int a_totalPassengers);
		~catamaran();

	};


}