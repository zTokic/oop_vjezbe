#include<iostream>
#include<string>
#include<vector>
#include"A_Vehicle.h"
#include"A_C_watercraft.h"
#include"A_D_aircraft.h"
#include"A_C-D_1_Seaplane.h"

using namespace std;
using namespace oop;

seaplane::seaplane(unsigned int a_totalPassengers) {

	cout << "Constructing seaplane" << endl;
	totalPassengers = a_totalPassengers;
	//vehicleType = aircraft::Type() + " - " + watercraft::Type();
	vehicleType = aircraft::vehicleType + " - " + watercraft::vehicleType;
}


seaplane::~seaplane() {

	cout << "Destructing seaplane" << endl;

}

unsigned seaplane::Passengers() {

	return totalPassengers;

}

string seaplane::Type() {

	return vehicleType;

}

string seaplane::Name() {

	return "Seaplane";

}