#include<iostream>
#include<string>
#include<vector>
#include"A_Vehicle.h"
#include"A_C_watercraft.h"
#include"A_C_2_Catamaran.h"

using namespace std;
using namespace oop;

catamaran::catamaran(unsigned int a_totalPassengers) {

	cout << "Constructing catamaran" << endl;
	totalPassengers = a_totalPassengers;

}


catamaran::~catamaran() {

	cout << "Destructing catamaran" << endl;

}

unsigned catamaran::Passengers() {

	return totalPassengers;

}

string catamaran::Name() {

	return "Catamaran";

}