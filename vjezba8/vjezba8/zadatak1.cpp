/*
 Definirajte interface (apstraktnu klasu) vehiclesa metodama type koja vrac astring koji predstavlja vrstu prijevoznog sredstva land,  water ili air
i passengers koja vraca broj putnika (unsigned).
 Izvedite klase kao na slici.
 Klase land vehicle, watercraft i aircraft implementiraju metodu type, a klase bike, car, catamaran, ferry i seaplane metodu passengers. 
Pretpostavljamo da bicikl ima jednog putnika, auto 5, catamaran i seaplane u konstruktoru dobije broj putnika, a ferryu konstruktoru dobije broj putnika, bicikli i automobila.
 Klasa seaplane izvedena je iz klasa watercraft i aircraft.
 Napisite klasu counter koja prima prijevozno sredstvo, prilikom dodavanja ispisuje informaciju o tipu prijevoznog sredstva i racuna ukupan broj putnika.
 U main funkciji napravite niz pointera nave hicleu kojem  ce biti razlicita prijevozna sredstva.   
Svako  prijevozno  sredstvo  posaljite  klasi counteri  ispisite  ukupan  brojputnika.
 Odvojite definicije klasa i implementacije u vise datoteka, te ih napisite unutar namespacea OSS.
*/



#include<iostream>
#include<vector>
#include<string>
#include"A_Vehicle.h"
#include"A_B_land_vehicle.h"
#include"A_B_1_Bike.h"
#include"A_B_2_Car.h"
#include"A_C_watercraft.h"
#include"A_C_1_Ferry.h"
#include"A_C_2_Catamaran.h"
#include"A_D_aircraft.h"
#include"A_C-D_1_Seaplane.h"
#include"Counter.h"

using namespace std;


int main(void) {
	using namespace oop; 
	
	counter c; 
	vehicle* v[] = { new bike, new car, new catamaran(30), new ferry(10, 5, 3),new seaplane(15) }; 
	size_t sz = sizeof v / sizeof v[0]; 

	cout << "-------------------------------" << endl;

	for (unsigned i = 0; i < sz; ++i)
		c.add(v[i]); 
		
	std::cout << "Ukupno " << c.total() << " putnika" << std::endl; 
	
	cout << "-------------------------------" << endl;
	
	for (unsigned i = 0; i < sz; ++i)
		delete v[i];
	
	cin.get();

}