#pragma once

#include<iostream>
#include<vector>
#include<string>
#include"A_B_Land_vehicle.h"


using namespace std;

namespace oop {

	class bike : public land_vehicle {

	protected:

		unsigned totalPassengers;

	public:

		unsigned Passengers() override;
		string Name() override;
		bike();
		~bike();

	};


}