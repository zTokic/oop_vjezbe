#pragma once

#include<iostream>
#include<string>
#include<vector>

using namespace std;
namespace oop {

	class vehicle {

	public:
		virtual string Type() = 0;
		virtual string Name() = 0;
		virtual unsigned Passengers() = 0;
	public:
		virtual ~vehicle() = 0;

	};

}