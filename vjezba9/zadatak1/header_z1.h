#pragma once

/*
Modificirajte template klasu pair tako da:
� implementirate sve preostale relacijske operatore
� implementirate operator pridruzivanja
� implementirate operatore << i >>
� implementirate swap member funkciju
� modificirate konstruktore (default i konstruktor s argumentima) tako da napravit samo jedan sa  parametrima koji moze primiti 0, 1 ili 2 argumenta
*/

#include<iostream>
using namespace std;

template<typename T1, typename T2>
class Pair {
	T1 first;
	T2 second;
public:
	//pair() : first(T1()), second(T2()) {}
	Pair(const T1& t1 = 0, const T2& t2 = 0) : first(t1), second(t2) { cout << "orig" << endl; }
	Pair(const Pair<T1, T2>& other) : first(other.first), second(other.second) { cout << "origC" << endl; }
	
	bool operator== (const Pair<T1, T2>& other) const {
		return first == other.first && second == other.second;
	}
	bool operator!= (const Pair<T1, T2>& other) const {
		return !(first == other.first && second == other.second);
	}
	bool operator< (const Pair<T1, T2>& other) const {
		return (first < other.first) || ((first == other.first) && (second < other.second));
	}
	bool operator<= (const Pair<T1, T2>& other) {
		return !(second < first);
	}
	bool operator> (const Pair<T1, T2>& other) {
		return (second < first);
	}
	bool operator>= (const Pair<T1, T2>& other) {
		return !(first < second);
	}
	void operator= (const Pair<T1, T2>& other){
		first = other.first;
		second = other.second;
	}
	friend ostream& operator<< (ostream& output, const Pair<T1, T2>& other){
		output << other.first << ' ' << other.second << endl;
		return output;
	}
	friend istream& operator>> (istream& input, const Pair<T1, T2>& other){
		input >> other.first >> other.second;
		return input;
	}
	void Swap(Pair<T1, T2>& other) {
		pair tempPair = first;
		first = second;
		second = tempPair;
	}
};


template<>
class Pair<char*, char*> {
	char* first;
	char* second;
public:

	Pair(char* t1 = new char, char* t2=new char) : first(t1), second(t2) { cout << "orig" << endl; }


	bool operator== (const Pair<char*, char*>& other) const {
		return (strcmp(first, other.first) == 0 && strcmp(second, other.second) == 0);
	}

	bool operator!= (const Pair<char*, char*>& other) const {
		return (!(strcmp(first, other.first) == 0 && strcmp(second, other.second) == 0));
	}
	bool operator< (const Pair<char*, char*>& other) const {
		return strcmp(first, other.first) < 0;
	}
	bool operator<= (const Pair<char*, char*>& other) {
		return strcmp(first, other.first) <= 0;
	}
	bool operator> (const Pair<char*, char*>& other) {
		return strcmp(first, other.first) > 0;
	}
	bool operator>= (const Pair<char*, char*>& other) {
		return strcmp(first, other.first) >= 0;
	}
	void operator= (const Pair<char*, char*>& other) {
		first = other.first;
		second = other.second;
	}
	friend ostream& operator<< (ostream& output, const Pair<char*, char*>& other) {
		output << other.first << ' ' << other.second << endl;
		return output;
	}
	friend istream& operator>> (istream& input, const Pair<char*, char*>& other) {
		input >> other.first >> other.second;
		return input;
	}
	void Swap(Pair<char*, char*>& other) {
	
		char* t1 = first;
		char* t2 = second;
		first = other.first;
		other.first = t1;
		second = other.second;
		other.second = t2;

	}
};



