

/*
Modificirajte template klasu pair tako da:
� implementirate sve preostale relacijske operatore
� implementirate operator pridruzivanja
� implementirate operatore << i >>
� implementirate swap member funkciju
� modificirate konstruktore (default i konstruktor s argumentima) tako da napravit samo jedan sa  parametrima koji moze primiti 0, 1 ili 2 argumenta
*/

#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
#include"header_z1.h"

using namespace std;

int main(){



	
	Pair<int, int> pp1 (5, 6);
	Pair<int, double> pp2(7, 8);
	Pair<int, int> pp3(9, 10);
	vector<Pair<int, int>> vBaza;

	
	//cout << "unesi prvi par" << endl;
	//cin >> pp1;
	cout << "Sortiranje..." << endl;
	sort(vBaza.begin(), vBaza.end());
	cin.get();
	cout << "Printanje pp2" << endl;
	cout << pp2;
	cin.get();
	


	

	Pair<char*, char*> p1, p2, p3;
	vector<Pair<char*, char*> > v;
	cout << "Unesi parove:" << endl;
	cin >> p1 >> p2 >> p3;

	cout << "Punjenje vektora..." << endl;
	v.push_back(p1);
	v.push_back(p2);
	v.push_back(p3);

	cin.get();

	cout << "Sortiranje..." << endl;
	sort(v.begin(), v.end());
	cin.get();

	cout << "Printanje..." << endl;
	vector<Pair<char*, char*> >::iterator it;
	for (it = v.begin(); it != v.end(); ++it) {
		cout << "--------------" << endl;
		cout << *it;
		cin.get();
	}
	cout << "--------------" << endl;

	bool manje = p1 < p2;
	cout << "Usporedba 'manje': " << manje << endl;
	cout << "Kopiranje p2 u p1.." << endl;
	cout << "Printanje starog p1" << endl;
	cout << p1 << endl;
	p1 = p2;
	cout << "Printanje novog p1" << endl;
	cout << p1 << endl;

	cin.get();




}