/*
Kreirajte klasu Array u kojoj se definira niz cijelih brojeva, cija velicina se odreduje u konstruktoru. 
Niz je alociran na heapu. Napisite potrebne konstruktore i destruktore.
Preopteretite sljedece operatore:
� operatore unosa i ispisa,
� operator =,
� operator +, koji spaja dva niza,
� operator -, ciji je rezultat novi niz u kojem su elementi prvog niza koji nisu u drugom nizu,
� operatore jednakosti i nejednakosti (clan po clan),
� operator [], kojem se pristupa clanovima niza po indexu (moguca je i promjena vrijednosti clana).
Napisite privatni staticki clan counter koji ce cuvati podatak o broju instanciranih objekata tipa Array i funkciju clan koja ce dohvacati vrijednosti countera.
Klasu iz prvog zadatka stavite unamespace OOP.
Napomena 1: U funkciji main testirajte svu funkcionalnost Arraya.
Napomena 2: U copykonstruktoru se, osim same vrijednosti pointera, kopiraju i vrijednosti niza (deep copy).
Napomena 3: Odvojite implementaciju i sucelje klase u razlicite datoteke.
*/

#include <iostream>
#include "array.h"

using namespace std;
using namespace oop;

int main() {

	int clanovi1, clanovi2;
	
	cout << "Unesi broj clanova prvog niza: ";
	cin >> clanovi1;
	cout << "Unesi broj clanova drugog niza: ";
	cin >> clanovi2;

	oop::Array popis1(clanovi1);
	oop::Array popis2(clanovi2);
	oop::Array popis3(0);
	oop::Array popis4(0);
	oop::Array popis5(0);

	cout << "Unesi " << clanovi1 << " elemenata prvog niza:" << endl;
	cin >> popis1;
	cout << "Unesi " << clanovi2 << " elemenata drugog niza:" << endl;
	cin >> popis2;
	
	int konstruktor = popis1.clan();
	cout << "Konstruktor je pokrenut -> " << konstruktor << " puta!" << endl;

	cout << "Ispis clanova prvog niza:" << endl << popis1 << endl;
	cout << "Ispis clanova drugog niza:" << endl << popis2 << endl;
	
	popis3 = popis1 + popis2;
	cout << "Ispis clanova spojenog niza:" << endl << popis3 << endl;

	popis4 = popis1 - popis2;
	cout << "Ispis clanova filtriranog niza:" << endl << popis4 << endl;

	cout << "Konstruktor je pokrenut -> " << konstruktor << " puta!" << endl;
	
	//cout << "izjednaceni popisi: " << oop::popis1 = popis2 << endl;
	bool vrijednost = (popis1 == popis2);
	bool vrijednost2 = (5 == 5);
	cout << "vrijednost1 " << vrijednost << endl;
	cout << "vrijednost2 " << vrijednost2 << endl;

	if (popis1 == popis2) {
		cout << "nizovi su jednaki(ISTINA)" << endl;
	}
	else {
		cout << "nizovi nisu jednaki(FALSE)" << endl;
	}


	cout << "-------------------------------------" << endl;
	if (popis1 != popis2) {
		cout << "nizovi su razliciti(ISTINA)" << endl;
	}
	else {
		cout << "nizovi su jednaki(FALSE)" << endl;
	}

	popis1[1] = 99;
	cout << "Novi popis1 je:" << popis1 << endl;

	//cout << "Instancirano je " << popis1.clan() << endl;
	//int konstruktor = popis1.clan();
	cout << "Konstruktor je pokrenut -> " << konstruktor << " puta!" << endl;
	
	cin.get();

}

