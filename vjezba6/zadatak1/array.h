
/*
Kreirajte klasu Array u kojoj se definira niz cijelih brojeva, cija velicina se odreduje u konstruktoru.
Niz je alociran na heapu. Napisite potrebne konstruktore i destruktore.
Preopteretite sljedece operatore:
� operatore unosa i ispisa,
� operator =,
� operator +, koji spaja dva niza,
� operator -, ciji je rezultat novi niz u kojem su elementi prvog niza koji nisu u drugom nizu,
� operatore jednakosti i nejednakosti (clan po clan),
� operator [], kojem se pristupa clanovima niza po indexu (moguca je i promjena vrijednosti clana).
*/

#pragma once
#include <iostream>

using namespace std;

namespace oop {

	class Array {

	private:

		int *m_niz;
		int m_velicina;
		static int m_clan;

	public:

		Array(int velicina);
		~Array();
		Array(const Array& arrayCopy);

		// overloaded the << operator
		friend istream &operator>> (istream &input, Array &temp) {

			for (int i = 0; i < temp.m_velicina; i++) {
				cout << "unesi clan niza" << endl;
				input >> temp.m_niz[i];
			}

			return input;
		}

		// overloaded the << operator
		friend ostream &operator<< (ostream &output, Array &temp) {
			for (int i = 0; i < temp.m_velicina; i++) {
				output << temp.m_niz[i] << " ";
			}
			cin.get();
			return output;

		}

		Array operator+ (Array& drugi);
		Array operator- (Array& drugi);
		bool operator== (Array& drugi);
		bool operator!= (Array& drugi);
		int &operator[] (int index);
		int clan();


	};
		

}

