/*
Kreirajte klasu Array u kojoj se definira niz cijelih brojeva, cija velicina se odreduje u konstruktoru.
Niz je alociran na heapu. Napisite potrebne konstruktore i destruktore.
Preopteretite sljedece operatore:
� operatore unosa i ispisa,
� operator =,
� operator +, koji spaja dva niza,
� operator -, ciji je rezultat novi niz u kojem su elementi prvog niza koji nisu u drugom nizu,
� operatore jednakosti i nejednakosti (clan po clan),
� operator [], kojem se pristupa clanovima niza po indexu (moguca je i promjena vrijednosti clana).
*/

#include <iostream>
#include "array.h"

using namespace std;
using namespace oop;


int Array::m_clan = 0;

Array::Array(int velicina) {

	m_niz = new int[velicina];
	m_velicina = velicina;
	m_clan++;

}


Array::~Array() {

	//delete[] m_niz;
	cout << "pokrenut destruktor" << endl;
}

Array::Array(const Array &arrayCopy) {

	m_velicina = arrayCopy.m_velicina;
	m_niz = new int[m_velicina];
	for (int i = 0; i < m_velicina; i++) {
		m_niz[i] = arrayCopy.m_niz[i];
	}
	//m_clan++;

}

Array Array::operator+ (Array& drugi) {

	int velicina = m_velicina + drugi.m_velicina;
	oop::Array spojeni(velicina);
	int index = m_velicina;

	for (int i = 0; i < m_velicina; i++) {
		spojeni.m_niz[i] = m_niz[i];
	}
	for (int i = 0; i < drugi.m_velicina; i++) {
		spojeni.m_niz[index] = drugi.m_niz[i];
		index++;
	}
	return spojeni;
}

Array Array::operator- (Array& drugi) {

	int velicinaPrvi = m_velicina;
	int velicinaDrugi = drugi.m_velicina;
	//provjera za alokaciju
	int brojRazlicitih;
	int velicinaNova = 0;
	for (int i = 0; i < velicinaPrvi; i++) {
		brojRazlicitih = 0;
		for (int j = 0; j < velicinaDrugi; j++) {
			if (m_niz[i] != drugi.m_niz[j]) {
				brojRazlicitih++;
			}
		}
		if (brojRazlicitih == velicinaDrugi) {
			velicinaNova++;
		}
	}
	//alokacija i punjenje niza
	oop::Array oduzeti(velicinaNova);
	int k = 0;
	for (int i = 0; i < velicinaPrvi; i++) {
		brojRazlicitih = 0;
		for (int j = 0; j < velicinaDrugi; j++) {
			if (m_niz[i] != drugi.m_niz[j]) {
				brojRazlicitih++;
			}
		}
		if (brojRazlicitih == velicinaDrugi) {
			oduzeti.m_niz[k] = m_niz[i];
			k++;
		}
	}

	return oduzeti;
}






bool Array::operator== (Array& drugi) {

	int velicinaPrvi = m_velicina;
	int velicinaDrugi = drugi.m_velicina;

	int brojistih = 0;
	for (int i = 0; i < velicinaPrvi; i++) {
		if (velicinaPrvi != velicinaDrugi) {
			cout << "NISU ISTE VELICINE" << endl;
			break;
		}
		else if (m_niz[i] == drugi.m_niz[i]) {
			brojistih++;
		}
		
	}
	if (brojistih == velicinaPrvi) {
		return true;
	}
	else
		return false;

}

bool Array::operator!= (Array& drugi) {

	int velicinaPrvi = m_velicina;
	int velicinaDrugi = drugi.m_velicina;

	int brojistih = 0;
	for (int i = 0; i < velicinaPrvi; i++) {
		if (velicinaPrvi != velicinaDrugi) {
			cout << "NISU ISTE VELICINE" << endl;
			break;
		}
		else if (m_niz[i] == drugi.m_niz[i]) {
			brojistih++;
		}
	}
	if (brojistih == velicinaPrvi) {
		return false;
	}
	else
		return true;
}

int &Array::operator[] (int index)
{
	if (index >= m_velicina) {
		cout << "Ne postoji broj na indexu" << endl;
		exit(0);
	}
	
	return m_niz[index];
}


int Array::clan() {

	return m_clan;

}





