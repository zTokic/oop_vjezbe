#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "ZooAnimal7.h"
#include "Bird.h"


class GriffonVulture : public Bird {

protected:

	double m_kolicinaHrane;

public:
	GriffonVulture(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek, double vrijemeInkubacije, double prosjecnaTemperaturaZivotinje, double kolicinaHrane);
	double GetKolicinaHrane() const;
	void IspisiZivotinju(ostream &output) const override;
	friend ostream& operator<<(ostream& output, const GriffonVulture& temp);

};

