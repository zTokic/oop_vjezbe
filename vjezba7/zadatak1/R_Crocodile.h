#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "ZooAnimal7.h"
#include "R_Reptile.h"


class Crocodile : public Reptile {

protected:

	double m_kolicinaHrane;

public:
	Crocodile(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek, double vrijemeInkubacije, double prosjecnaTemperaturaZivotinje, double kolicinaHrane);
	double GetKolicinaHrane() const;
	void IspisiZivotinju(ostream &output) const override;
	friend ostream& operator<<(ostream& output, const Crocodile& temp);

};

