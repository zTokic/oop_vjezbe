#include<iostream>
#include<vector>
#include<string>
#include "ZooAnimal7.h"
#include "R_Reptile.h"
#include "R_Crocodile.h"


using namespace std;

Crocodile::Crocodile(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek, double vrijemeInkubacije, double prosjecnaTemperaturaOkoline, double kolicinaHrane)
	: Reptile(vrsta, ime, godinaRodjenja, brojKaveza, brojDnevnihObroka, ocekivaniZivotniVijek, vrijemeInkubacije, prosjecnaTemperaturaOkoline) {

	m_kolicinaHrane = kolicinaHrane;

}

void Crocodile::IspisiZivotinju(ostream& output) const {
	output << "Nacin razmnozavanja: " << m_nacinRazmnozavanja << endl
		<< "Vrijeme inkubacije: " << m_vrijemeInkubacije << " mjeseci" << endl
		<< "Temperatura okoline: " << m_prosjecnaTemperaturaOkoline << " C" << endl
		<< "Broj obroka po danu: " << m_brojDnevnihObroka << endl
		<< "Kolicina hrane po obroku: " << m_kolicinaHrane << " kg" << endl
		<< "Ukupna kolicina hrane po danu: " << m_brojDnevnihObroka * m_kolicinaHrane << " kg" << endl;
}



double Crocodile::GetKolicinaHrane() const {

	return m_kolicinaHrane;
}


ostream& operator<<(ostream& output, const Crocodile& temp) {
	output << "Ime: " << temp.m_ime << endl
		<< "Vrsta: " << temp.m_vrsta << endl
		<< "Broj kaveza: " << temp.m_brojKaveza << endl
		<< "Ocekivani zivotni vijek: " << temp.m_ocekivaniZivotniVijek << " god" << endl;
	temp.IspisiZivotinju(output);
	output << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << endl;

	return output;
}


