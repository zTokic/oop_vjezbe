#include<iostream>
#include<vector>
#include<string>
#include "ZooAnimal7.h"
#include "Bird.h"
#include "GriffonVulture.h"


using namespace std;

GriffonVulture::GriffonVulture(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek, double vrijemeInkubacije, double prosjecnaTemperaturaZivotinje, double kolicinaHrane)
	: Bird(vrsta, ime, godinaRodjenja, brojKaveza, brojDnevnihObroka, ocekivaniZivotniVijek, vrijemeInkubacije, prosjecnaTemperaturaZivotinje) {

	m_kolicinaHrane = kolicinaHrane;

}

void GriffonVulture::IspisiZivotinju(ostream& output) const {
	output << "Nacin razmnozavanja: " << m_nacinRazmnozavanja << endl
		<< "Vrijeme inkubacije: " << m_vrijemeInkubacije << " mjeseci" << endl
		<< "Temperatura tijela: " << m_prosjecnaTemperaturaZivotinje << " C" << endl
		<< "Broj obroka po danu: " << m_brojDnevnihObroka << endl
		<< "Kolicina hrane po obroku: " << m_kolicinaHrane << " kg" << endl
		<< "Ukupna kolicina hrane po danu: " << m_brojDnevnihObroka * m_kolicinaHrane << " kg" << endl;
}



double GriffonVulture::GetKolicinaHrane() const {

	return m_kolicinaHrane;
}


ostream& operator<<(ostream& output, const GriffonVulture& temp) {
	output << "Ime: " << temp.m_ime << endl
		<< "Vrsta: " << temp.m_vrsta << endl
		<< "Broj kaveza: " << temp.m_brojKaveza << endl
		<< "Ocekivani zivotni vijek: " << temp.m_ocekivaniZivotniVijek << " god" << endl;
	temp.IspisiZivotinju(output);
	output << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << endl;

	return output;
}


