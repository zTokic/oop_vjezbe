#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "ZooAnimal7.h"
#include "Mammal.h"


class Monkey : public Mammal {

protected:

	double m_kolicinaHrane;

public:
	Monkey(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek, double gestacijskiPeriod, double prosjecnaTemperaturaZivotinje, double kolicinaHrane);
	double GetKolicinaHrane() const;
	void IspisiZivotinju(ostream &output) const override;
	friend ostream& operator<<(ostream& output, const Monkey& temp);

};

