
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include "ZooAnimal7.h"

using namespace std;

/*
ZooAnimal::ZooAnimal() {

}
*/

ZooAnimal::ZooAnimal(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek) {

	cout << "Konstruktor..." << endl;
	m_vrsta = vrsta;
	m_ime = ime;
	m_godinaRodjenja = godinaRodjenja;
	m_brojKaveza = brojKaveza;
	m_brojDnevnihObroka = brojDnevnihObroka;
	m_ocekivaniZivotniVijek = ocekivaniZivotniVijek;
	m_masaPodaci = new Masa[m_ocekivaniZivotniVijek * 2];

	for (int i = 0; i < m_ocekivaniZivotniVijek * 2; i++) {//ispunit nulama radi provjere
		m_masaPodaci[i].m_godinaVaganja = 0;
		m_masaPodaci[i].m_masaZivotinje = 0;
	}

}

ZooAnimal::~ZooAnimal() {

	cout << "Destruktor..." << endl;

}

ZooAnimal::ZooAnimal(const ZooAnimal& copyZooAnimal) {

	cout << "Copy Konstruktor..." << endl;
	m_vrsta = copyZooAnimal.m_vrsta;
	m_ime = copyZooAnimal.m_ime;
	m_godinaRodjenja = copyZooAnimal.m_godinaRodjenja;
	m_brojKaveza = copyZooAnimal.m_brojKaveza;
	m_brojDnevnihObroka = copyZooAnimal.m_brojDnevnihObroka;
	m_ocekivaniZivotniVijek = copyZooAnimal.m_ocekivaniZivotniVijek;
	m_masaPodaci = new Masa[m_ocekivaniZivotniVijek * 2];
	m_masaPodaci = copyZooAnimal.m_masaPodaci;

}

void ZooAnimal::PromjenaDnevnihObroka(bool promjena) {

	if (promjena)
		m_brojDnevnihObroka++;
	else
		m_brojDnevnihObroka--;

}

void ZooAnimal::DodajPodatkeOMasi(int godinaVaganja, int masaZivotinje) {

	int provjeraGodine = godinaVaganja;
	for (int i = 0; i < m_ocekivaniZivotniVijek * 2; i++) {
		if (provjeraGodine == m_masaPodaci[i].m_godinaVaganja) {
			cout << "Nedozvoljen unos. Godina vec postoji!" << endl;
			break;
		}
		else {
			if (m_masaPodaci[i].m_godinaVaganja == 0) {//provjera jel prazno
				m_masaPodaci[i].m_godinaVaganja = godinaVaganja;
				m_masaPodaci[i].m_masaZivotinje = masaZivotinje;
				break;
			}
		}
	}

}

void ZooAnimal::IspisiObjekt() {

	cout << "Vrsta: " << m_vrsta << endl;
	cout << "Ime: " << m_ime << endl;
	cout << "Godina rodjenja: " << m_godinaRodjenja << endl;
	cout << "Broj kaveza: " << m_brojKaveza << endl;
	cout << "Broj dnevnih obroka: " << m_brojDnevnihObroka << endl;
	cout << "Ocekivani zivotni vijek: " << m_ocekivaniZivotniVijek << endl;
	cout << "----------------------------------" << endl;
	for (int i = 0; i < m_ocekivaniZivotniVijek * 2; i++) {
		if (m_masaPodaci[i].m_godinaVaganja != 0) {
			cout << "Godina vaganja: " << m_masaPodaci[i].m_godinaVaganja << endl;
			cout << "Masa: " << m_masaPodaci[i].m_masaZivotinje << endl;
		}
		else if (m_masaPodaci[i].m_godinaVaganja == 0) {
			cout << "__________________________________" << endl;
			break;
		}
	}
}


bool ZooAnimal::ProvjeraMase() {

	//time_t t = time(NULL);
	//tm* timePtr = localtime(&t);
	//int godinaTrenutna = timePtr->tm_year + 1900;
	int godinaTrenutna = 2019;
	int razlikaMasa;
	int prijasnjaMasa;
	for (int i = 0; i < m_ocekivaniZivotniVijek * 2; i++) {
		if (godinaTrenutna == m_masaPodaci[i].m_godinaVaganja) {//trazenje trenutne godine u nizu
			razlikaMasa = m_masaPodaci[i].m_masaZivotinje - m_masaPodaci[i - 1].m_masaZivotinje;
			prijasnjaMasa = m_masaPodaci[i - 1].m_masaZivotinje;
		}
	}

	int postotak = razlikaMasa * (100 / prijasnjaMasa);

	if (postotak < 10)
		return false;
	else if (postotak < 0 && abs(postotak)>10)
		return true;

}

int ZooAnimal::GetBrojDnevnihObroka() {
	return m_brojDnevnihObroka;
}