/*
Iz klase ZooAnimal izvedite klase
Mammal (sisavci), Bird (ptice) i Reptile (gmazovi).

Iz njih dalje izvedite klase
Tiger, Monkey, Elephant, GriffonVulture (bjeloglavi sup), Owl (sova), Crocodile i Turtle (kornjaca je gmaz).

Sisavci nose mlade i toplokrvne su zivotinje. Njima dodajte gestacijski period i prosjecnu temperaturu zivotinje kao clan klase.

Ptice nose jaja i toplokrvne su zivotinje. Njima dodajte vrijeme inkubacije i prosjecnu temperaturu tijela.

Gmazovi (uglavnom) nose jaja i hladnokrvne su zivotinje. Njima dodajte vrijeme inkubacije i temperaturu okoline.

Svaka zivotinja ima propisanu kolicinu hrane u jednom obroku.

(a) Napisite potrebne clanove, konstruktore/destruktore.
(b) Napisite preopterecene operatore << i >> tako da se za svaku zivotinju ispisuju sljedeci podaci:
ime, vrsta, poruku o nacinu razmnozavanja (radanje ili polaganje jaja), gestacijski period tj. vrijeme inkubacije, temperaturu tijela ili okoline.
(c) U main funkciji napunite vektor pojedinacnim zivotinjama.
(d) Ispisite podatke o zivotinji, te ukupnu kolicinu hrane (broj obroka je dodan u ZooAnimal klasu u vjezbi 5) koji zoo vrt treba osigurati zivotinjama u jednom danu.
Odvojite definicije klasa i implementacije u vise datoteka, te ih napisite unutar namespacea OSS.

*/


#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include "ZooAnimal7.h"
#include "Mammal.h"

using namespace std;



Mammal::Mammal(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek, double gestacijskiPeriod, double prosjecnaTemperaturaZivotinje) 
	: ZooAnimal(vrsta, ime, godinaRodjenja, brojKaveza, brojDnevnihObroka, ocekivaniZivotniVijek) {

	m_gestacijskiPeriod = gestacijskiPeriod;
	m_prosjecnaTemperaturaZivotinje = prosjecnaTemperaturaZivotinje;
	m_nacinRazmnozavanja = "Radjanje";
	
}



