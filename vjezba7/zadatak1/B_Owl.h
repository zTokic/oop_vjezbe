#pragma once

#include <iostream>
#include <string>
#include <vector>
#include "ZooAnimal7.h"
#include "Bird.h"


class Owl : public Bird {

protected:

	double m_kolicinaHrane;

public:
	Owl(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek, double vrijemeInkubacije, double prosjecnaTemperaturaZivotinje, double kolicinaHrane);
	double GetKolicinaHrane() const;
	void IspisiZivotinju(ostream &output) const override;
	friend ostream& operator<<(ostream& output, const Owl& temp);

};

