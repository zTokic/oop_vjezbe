#include<iostream>
#include<vector>
#include<string>
#include "ZooAnimal7.h"
#include "Mammal.h"
#include "Monkey.h"


using namespace std;

Monkey::Monkey(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek, double gestacijskiPeriod, double prosjecnaTemperaturaZivotinje, double kolicinaHrane)
	: Mammal(vrsta, ime, godinaRodjenja, brojKaveza, brojDnevnihObroka, ocekivaniZivotniVijek, gestacijskiPeriod, prosjecnaTemperaturaZivotinje) {

	m_kolicinaHrane = kolicinaHrane;

}

void Monkey::IspisiZivotinju(ostream& output) const {
	output << "Nacin razmnozavanja: " << m_nacinRazmnozavanja << endl
		<< "Gestacijski period: " << m_gestacijskiPeriod << " mjeseci" << endl
		<< "Temperatura tijela: " << m_prosjecnaTemperaturaZivotinje << " C" << endl
		<< "Broj obroka po danu: " << m_brojDnevnihObroka << endl
		<< "Kolicina hrane po obroku: " << m_kolicinaHrane << " kg" << endl
		<< "Ukupna kolicina hrane po danu: " << m_brojDnevnihObroka * m_kolicinaHrane << " kg" << endl;
}



double Monkey::GetKolicinaHrane() const {

	return m_kolicinaHrane;
}


ostream& operator<<(ostream& output, const Monkey& temp) {
	output << "Ime: " << temp.m_ime << endl
		<< "Vrsta: " << temp.m_vrsta << endl
		<< "Broj kaveza: " << temp.m_brojKaveza << endl
		<< "Ocekivani zivotni vijek: " << temp.m_ocekivaniZivotniVijek << " god" << endl;
	temp.IspisiZivotinju(output);
	output << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << endl;

	return output;
}


