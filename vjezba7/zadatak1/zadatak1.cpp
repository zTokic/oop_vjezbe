/*
Iz klase ZooAnimal izvedite klase
Mammal (sisavci), Bird (ptice) i Reptile (gmazovi).

Iz njih dalje izvedite klase
Tiger, Monkey, Elephant, GriffonVulture (bjeloglavi sup), Owl (sova), Crocodile i Turtle (kornjaca je gmaz).

Sisavci nose mlade i toplokrvne su zivotinje. Njima dodajte gestacijski period i prosjecnu temperaturu zivotinje kao clan klase.
Ptice nose jaja i toplokrvne su zivotinje. Njima dodajte vrijeme inkubacije i prosjecnu temperaturu tijela.
Gmazovi (uglavnom) nose jaja i hladnokrvne su zivotinje. Njima dodajte vrijeme inkubacije i temperaturu okoline.
Svaka zivotinja ima propisanu kolicinu hrane u jednom obroku.

(a) Napisite potrebne clanove, konstruktore/destruktore.
(b) Napisite preopterecene operatore << i >> tako da se za svaku zivotinju ispisuju sljedeci podaci:
ime, vrsta, poruku o nacinu razmnozavanja (radanje ili polaganje jaja), gestacijski period tj. vrijeme inkubacije, temperaturu tijela ili okoline.
(c) U main funkciji napunite vektor pojedinacnim zivotinjama.
(d) Ispisite podatke o zivotinji, te ukupnu kolicinu hrane (broj obroka je dodan u ZooAnimal klasu u vjezbi 5) koji zoo vrt treba osigurati zivotinjama u jednom danu.
Odvojite definicije klasa i implementacije u vise datoteka, te ih napisite unutar namespacea OSS.

*/



#include <iostream>
#include "ZooAnimal7.h"
#include "Mammal.h"
#include "Tiger.h"
#include "Monkey.h"
#include "Elephant.h"
#include "GriffonVulture.h"
#include "B_Owl.h"
#include "R_Crocodile.h"
#include "R_Turtle.h"





using namespace std;

int main() {

	vector<ZooAnimal> animals;

	//no defalut constructor exists for class ZooAnimal
	//ZooAnimal medvid;	
	ZooAnimal zivotinja1("medvjed", "ivan", 2005, 0, 4, 30);
	ZooAnimal zivotinja2("jelen", "mate", 2005, 1, 4, 30);
	//ZooAnimal zivotinja3("deva", "ivan", 2005, 2, 4, 30);
	//animals.push_back(zivotinja1);
	//animals.push_back(zivotinja2);
	//animals.push_back(zivotinja3);

	zivotinja1.DodajPodatkeOMasi(2018, 15);
	zivotinja1.DodajPodatkeOMasi(2019, 33);
	zivotinja1.IspisiObjekt();

	bool dodajSmanji = zivotinja1.ProvjeraMase();

	zivotinja1.PromjenaDnevnihObroka(dodajSmanji);
	zivotinja1.IspisiObjekt();
	cout << "printanje__________" << endl;
	animals.push_back(zivotinja1);
	animals.push_back(zivotinja2);
	for (int i = 0; i < animals.size(); i++) {
		animals[i].IspisiObjekt();
	}


	cout << "NOVOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\n" << endl;
	
	Tiger tigar("Tigar", "Marko", 2006, 3, 6, 25, 0.7, 32.5, 14.2);
	animals.push_back(tigar);
	tigar.IspisiObjekt();

	vector<ZooAnimal*> zivotinje = {
		new Tiger("Tigric", "Marko", 2006, 0, 6, 25, 7, 36.2, 14.2),
		new Monkey("Majmun", "Jure", 2010, 1, 3, 20, 9, 37.1, 0.5),
		new Elephant("Slon", "Mladen", 1999, 2, 5, 18, 14, 36.9, 80),
		new GriffonVulture("Bjeloglavi sup", "Toni", 2010, 3, 7, 35, 5, 36.1, 0.2),
		new Owl("Sova", "Mudra", 2015, 4, 7, 20, 0.7, 36.4, 0.1),
		new Crocodile("Krokodil", "Mirko", 1985, 5, 2, 80, 1, 32.1, 40),
		new Turtle("Bjeloglavi sup", "Toni", 1963, 5, 4, 200, 4, 33.8, 0.15)

	};


	
	cout << tigar;
	cout << "******************************\n";
	
	size_t length = zivotinje.size();
	for (size_t i = 0; i < length; i++) {
		//cout << zivotinje[i]->;
	}

	///////////////////////////////////////////////////////////
	// ovo radi
	Owl o ("Sova", "Mudra", 2015, 4, 7, 20, 0.7, 36.4, 0.1);
	cout << o;

	cout << "LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL\n";


	Elephant *slon = new Elephant("Slon", "Mladen", 1999, 2, 5, 18, 14, 36.9, 80);
	//cin >> *el;
	cout << *slon;
	cout << "LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL\n";
	
	
	///////////////////////////////////////////////////////////
	// ne radi, izbacuje adrese ka i ono gore
	Bird *sup = new GriffonVulture("Bjeloglavi sup", "Toni", 2010, 3, 7, 35, 5, 36.1, 0.2);
	Bird *sova = new Owl("Sova", "Mudra", 2015, 4, 7, 20, 0.7, 36.4, 0.1);
	Reptile *guster = new Crocodile("Krokodil", "Mirko", 1985, 5, 2, 80, 1, 32.1, 40);

	vector<ZooAnimal*> zivotinje2;

	zivotinje2.push_back(slon);
	zivotinje2.push_back(sup);
	zivotinje2.push_back(sova);
	zivotinje2.push_back(guster);

	for (int i = 0; i < zivotinje2.size(); i++) {
		cout << zivotinje2[i] << endl;
	}


	cout << tigar;


	cin.get();


}