
#pragma once


#include <iostream>
#include <string>
#include <vector>

using namespace std;


class Masa {

public:
	int m_godinaVaganja;
	int m_masaZivotinje;

};

class ZooAnimal {

protected:

	string m_vrsta;
	string m_ime;
	int m_godinaRodjenja;
	int m_brojKaveza;
	int m_brojDnevnihObroka;
	int m_ocekivaniZivotniVijek;
	Masa* m_masaPodaci;

public:

	//ZooAnimal();
	ZooAnimal(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek);
	~ZooAnimal();
	ZooAnimal(const ZooAnimal& copyZooAnimal);
	void PromjenaDnevnihObroka(bool promjena);
	void DodajPodatkeOMasi(int godinaVaganja, int masaZivotinje);
	void IspisiObjekt();
	bool ProvjeraMase();
	int GetBrojDnevnihObroka();
	virtual void IspisiZivotinju(ostream &output) const {}
	virtual void test(ostream &output) const {}
};
