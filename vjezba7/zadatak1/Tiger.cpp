#include<iostream>
#include<vector>
#include<string>
#include "ZooAnimal7.h"
#include "Mammal.h"
#include "Tiger.h"


using namespace std;

Tiger::Tiger(string vrsta, string ime, int godinaRodjenja, int brojKaveza, int brojDnevnihObroka, int ocekivaniZivotniVijek, double gestacijskiPeriod, double prosjecnaTemperaturaZivotinje, double kolicinaHrane) 
	: Mammal(vrsta, ime, godinaRodjenja, brojKaveza, brojDnevnihObroka, ocekivaniZivotniVijek, gestacijskiPeriod, prosjecnaTemperaturaZivotinje) {

	m_kolicinaHrane = kolicinaHrane;

}

void Tiger::IspisiZivotinju(ostream& output) const {
	output << "Nacin razmnozavanja: " << m_nacinRazmnozavanja << endl
		<< "Gestacijski period: " << m_gestacijskiPeriod << " mjeseci" << endl
		<< "Temperatura tijela: " << m_prosjecnaTemperaturaZivotinje << " C" << endl
		<< "Broj obroka po danu: " << m_brojDnevnihObroka << endl
		<< "Kolicina hrane po obroku: " << m_kolicinaHrane << " kg" << endl
		<< "Ukupna kolicina hrane po danu: " << m_brojDnevnihObroka*m_kolicinaHrane << " kg" << endl;
}


void Tiger::test(ostream& output) const {
	output << "Ime: " << m_ime << endl
		<< "Vrsta: " << m_vrsta << endl
		<< "Broj kaveza: " << m_brojKaveza << endl
		<< "Ocekivani zivotni vijek: " << m_ocekivaniZivotniVijek << " god" << endl
		<< "Nacin razmnozavanja: " << m_nacinRazmnozavanja << endl
		<< "Gestacijski period: " << m_gestacijskiPeriod << " mjeseci" << endl
		<< "Temperatura tijela: " << m_prosjecnaTemperaturaZivotinje << " C" << endl
		<< "Broj obroka po danu: " << m_brojDnevnihObroka << endl
		<< "Kolicina hrane po obroku: " << m_kolicinaHrane << " kg" << endl
		<< "Ukupna kolicina hrane po danu: " << m_brojDnevnihObroka * m_kolicinaHrane << " kg" << endl;

}

double Tiger::GetKolicinaHrane() const {

	return m_kolicinaHrane;
}


ostream& operator<<(ostream& output, const Tiger& temp) {
	/*output << "Ime: " << temp.m_ime << endl
		<< "Vrsta: " << temp.m_vrsta << endl
		<< "Broj kaveza: " << temp.m_brojKaveza << endl
		<< "Ocekivani zivotni vijek: " << temp.m_ocekivaniZivotniVijek << " god" << endl;
	temp.IspisiZivotinju(output);*/
	temp.test(output);
	output << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << endl;
		
	return output;
}


