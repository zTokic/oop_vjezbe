#include "pch.h"
#include <iostream>

/*
U nizu od N cijelih brojeva, nalaze se duplikati. Pronadi ih i ispisi koliko puta se
ponavljaju! Moguce su samo vrijednosti 1-9
*/

int main() {

	int niz[] = { 1,2,3,4,4,5,6,7,7,7,7,7,7,7,8,9,7 };
	int duljina = sizeof(niz) / sizeof(int);
	int i, j, brojac;

	for (i = 1; i <= 9; i++) {
		brojac = 0;
		for (j = 0; j < duljina; j++) {
			if (i == niz[j]) {
				brojac++;
			}
		}
		std::cout << "Broj " << i << " se ponavlja " << brojac << "x.\n";
	}
}