#include "pch.h"
#include <iostream>
#include <string>
#include <vector>
#include <random>

/*
Napisite program za vodenje evidencije i statisticku analizu za razred od 20 studenata.
Informacije o svakom studentu sadrze ID string, ime, spol, ocjene kvizova (2 kviza po semestru), ocjenu na sredini semestra (mid-term score), ocjenu na kraju semestra
(final score) i ukupan broj bodova koji je zbroj kviza1, kviza2, ocjene na sredini i ocjene na kraju semestra (total score). Kreiraj strukturu student i napisi implementaciju 
za izbornik ispod:
� Dodaj novi zapis
� Ukloni zapis
� Azuriraj zapis
� Prikazi sve zapise
� Izracunaj prosjek bodova za studenta
� Prikazi studenta s najvecim brojem bodova
� Prikazi studenta s najmanjim brojem bodova
� Pronadi studenta po ID-u
� Sortiraj zapise po broju bodova (total)
� Izlaz
Imati na umu da se korisniku ne smije dozvoliti unos za studenta s vec postoje cim ID-ijem, te slucajeve kad u nizu nema zapisa, kad korisnik upise ID koji ne postoji u nizu i slicno.
*/

struct student {
	std::string ID;
	std::string name;
	std::string sex;
	int test1;
	int test2;
	int midterm;
	int finscore;
	int totalscore;
};

int structSize = 4;


//DODAJ ZAPIS ---- CASE 1

void dodaj_novi_zapis(struct student s[20]) {

	if (structSize == 20) {
		std::cout << "Unesen je maksimalan broj studenata" << std::endl;
		return;
	}

	std::string temp;
	int i = structSize + 1;
	std::cout << "\nUNOS NOVOG STUDENTA:\n\n";
	std::cout << ">>>  ID: ";
	std::cin >> temp;
	//provjera ID
	for (int j = 0; j < structSize; j++) {
		if (temp.compare(s[j].ID) == 0) {
			std::cout << "\nGeska!!! ID vec postoji!!!" << std::endl;
			std::cout << "--------------------------\n";
			return;
		}
	}

	s[i].ID = temp;
	std::cout << ">>>  Name: ";
	std::cin >> s[i].name;
	std::cout << ">>>  Sex: ";
	std::cin >> s[i].sex;
	std::cout << ">>>  Test1: ";
	std::cin >> s[i].test1;
	std::cout << ">>>  Test2: ";
	std::cin >> s[i].test2;
	std::cout << ">>>  Mid-term score: ";
	std::cin >> s[i].midterm;
	std::cout << ">>>  Final score: ";
	std::cin >> s[i].finscore;
	//totalscore
	s[i].totalscore = s[i].test1 + s[i].test2 + s[i].midterm + s[i].finscore;
	
	structSize++;
	//std::cout << "\n\nSTRUCTSIZE: " << structSize << std::endl;
	
	std::cout << "\nPRIKAZ UNESENIH PODATAKA:\n";
	std::cout << "--------------------------\n";
	std::cout << ">>>  ID: " << s[i].ID << std::endl;
	std::cout << ">>>  Name: " << s[i].name << std::endl;
	std::cout << ">>>  Sex: " << s[i].sex << std::endl;
	std::cout << ">>>  Test1: " << s[i].test1 << std::endl;
	std::cout << ">>>  Test2: " << s[i].test2 << std::endl;
	std::cout << ">>>  Mid-term score: " << s[i].midterm << std::endl;
	std::cout << ">>>  Final score: " << s[i].finscore << std::endl;
	std::cout << ">>>  Total score: " << s[i].totalscore << std::endl;
	std::cout << "--------------------------\n\n";
}


//UKLONI ZAPIS ---- CASE 2

void ukloni_zapis(struct student s[20]) {

	std::string temp;
	std::cout << "\nUNESITE ID STUDENTA KOJEG ZELITE BRISATI:\n\n";
	std::cout << ">>>  ID: ";
	std::cin >> temp;
	//provjera ID
	/*
	for (int j = 0; j < structSize; j++) {
		if (temp.compare(s[j].ID) == 0) {
			std::cout << "\nGeska!!! ID postoji!!!" << std::endl;
			std::cout << "--------------------------\n";
			return;
		}
	}*/
	//brisanje
	for (int i = 0; i < structSize; i++) {
		if (temp.compare(s[i].ID) == 0) {
			for (int j = i + 1; j < structSize; j++) {
				s[j - 1] = s[j];
			}
			structSize -= 1;
		}
		else
			std::cout << "Ne postoji student sa trazenim ID-om" << std::endl;

	}
}


//AZURIRAJ ZAPIS ---- CASE 3

void azuriraj_zapis(struct student s[20]) {

	std::string temp;
	std::cout << "\nUNESITE ID STUDENTA KOJEG ZELITE AZURIRATI:\n\n";
	std::cout << ">>>  ID: ";
	std::cin >> temp;
	int i;
	
	//provjera ID
	for (int j = 0; j < structSize; j++) {
		if (temp.compare(s[j].ID) == 0) {
			i = j;
		}
	}

	s[i].ID = temp;
	std::cout << ">>>  Name: ";
	std::cin >> s[i].name;
	std::cout << ">>>  Sex: ";
	std::cin >> s[i].sex;
	std::cout << ">>>  Test1: ";
	std::cin >> s[i].test1;
	std::cout << ">>>  Test2: ";
	std::cin >> s[i].test2;
	std::cout << ">>>  Mid-term score: ";
	std::cin >> s[i].midterm;
	std::cout << ">>>  Final score: ";
	std::cin >> s[i].finscore;
	s[i].totalscore = s[i].test1 + s[i].test2 + s[i].midterm + s[i].finscore;


	std::cout << "\nPRIKAZ UNESENIH PODATAKA:\n";
	std::cout << "--------------------------\n";
	std::cout << ">>>  ID: " << s[i].ID << std::endl;
	std::cout << ">>>  Name: " << s[i].name << std::endl;
	std::cout << ">>>  Sex: " << s[i].sex << std::endl;
	std::cout << ">>>  Test1: " << s[i].test1 << std::endl;
	std::cout << ">>>  Test2: " << s[i].test2 << std::endl;
	std::cout << ">>>  Mid-term score: " << s[i].midterm << std::endl;
	std::cout << ">>>  Final score: " << s[i].finscore << std::endl;
	std::cout << ">>>  Total score: " << s[i].totalscore << std::endl;
	std::cout << "--------------------------\n\n";



}


//PRIKAZI SVE ZAPISE ---- CASE 4

void prikazi_zapis(struct student s[20]) {

	int i, povratak;
	std::cout << "\n\nPRIKAZ STUDENTSKIH PODATAKA:\n\n";

	for (i = 0; i < structSize; i++) {
		std::cout << ">>>  ID: " << s[i].ID << std::endl;
		std::cout << ">>>  Name: " << s[i].name << std::endl;
		std::cout << ">>>  Sex: " << s[i].sex << std::endl;
		std::cout << ">>>  Test1: " << s[i].test1 << std::endl;
		std::cout << ">>>  Test2: " << s[i].test2 << std::endl;
		std::cout << ">>>  Mid-term score: " << s[i].midterm << std::endl;
		std::cout << ">>>  Final score: " << s[i].finscore << std::endl;
		std::cout << ">>>  Total score: " << s[i].totalscore << std::endl;
		std::cout << "\n--------------------------\n\n";
	}
}


//PRIKAZ PROSJEKA BODOVA ---- CASE 5

void prosjek_bodova(struct student s[20]) {

	int prosjek, i, sum = 0;
	for (i = 0; i < structSize; i++) {
		sum += s[i].totalscore;
	}
	prosjek = sum / structSize;
	std::cout << "PROSJECAN BROJ BODOVA SVIH STUDENATA:  " << prosjek << std::endl;
}


//PRIKAZ STUDENTA SA NAJVISE BODOVA ---- CASE 6

void najvise_bodova(struct student s[20]) {

	int i=0, cnt;
	for (cnt = 1; cnt < structSize; cnt++) {
		if (s[cnt].totalscore > s[0].totalscore) {
			i = cnt;
		}
	}
	//std::cout << "indeks je " << i << std::endl;
	std::cout << "STUDENT SA NAJVISE BODOVA JE:\n" << std::endl;
	std::cout << "--------------------------\n";
	std::cout << ">>>  ID: " << s[i].ID << std::endl;
	std::cout << ">>>  Name: " << s[i].name << std::endl;/*
	std::cout << ">>>  Sex: " << s[i].sex << std::endl;
	std::cout << ">>>  Test1: " << s[i].test1 << std::endl;
	std::cout << ">>>  Test2: " << s[i].test2 << std::endl;
	std::cout << ">>>  Mid-term score: " << s[i].midterm << std::endl;
	std::cout << ">>>  Final score: " << s[i].finscore << std::endl;*/
	std::cout << ">>>  Total score: " << s[i].totalscore << std::endl;
	std::cout << "--------------------------\n\n";
}


//PRIKAZ STUDENTA SA NAJVISE BODOVA ---- CASE 7

void najmanje_bodova(struct student s[20]) {

	int i = 0, cnt;
	for (cnt = 1; cnt < structSize; cnt++) {
		if (s[cnt].totalscore < s[0].totalscore) {
			i = cnt;
		}
	}
	//std::cout << "indeks je " << i << std::endl;
	std::cout << "STUDENT SA NAJMANJE BODOVA JE:\n" << std::endl;
	std::cout << "--------------------------\n";
	std::cout << ">>>  ID: " << s[i].ID << std::endl;
	std::cout << ">>>  Name: " << s[i].name << std::endl;/*
	std::cout << ">>>  Sex: " << s[i].sex << std::endl;
	std::cout << ">>>  Test1: " << s[i].test1 << std::endl;
	std::cout << ">>>  Test2: " << s[i].test2 << std::endl;
	std::cout << ">>>  Mid-term score: " << s[i].midterm << std::endl;
	std::cout << ">>>  Final score: " << s[i].finscore << std::endl;*/
	std::cout << ">>>  Total score: " << s[i].totalscore << std::endl;
	std::cout << "--------------------------\n\n";
}


//PRETRAGA PO ID-u ---- CASE 8

void pretraga(struct student s[20]) {

	std::string temp;
	std::cout << "\n\nUNESITE ID ZA PRETRAGU:\n\n";
	std::cin >> temp;
	int i = 0;
	for (i = 0; i < structSize; i++) {
		if (temp.compare(s[i].ID) == 0) {
			std::cout << "\n--------------------------\n";
			std::cout << ">>>  ID: " << s[i].ID << std::endl;
			std::cout << ">>>  Name: " << s[i].name << std::endl;
			std::cout << ">>>  Sex: " << s[i].sex << std::endl;
			std::cout << ">>>  Test1: " << s[i].test1 << std::endl;
			std::cout << ">>>  Test2: " << s[i].test2 << std::endl;
			std::cout << ">>>  Mid-term score: " << s[i].midterm << std::endl;
			std::cout << ">>>  Final score: " << s[i].finscore << std::endl;
			std::cout << ">>>  Total score: " << s[i].totalscore << std::endl;
			std::cout << "--------------------------\n\n";
			break;
		}
		else
			std::cout << "Ne postoji student sa trazenim ID-om" << std::endl;
	}

}


//SORTIRANJE ---- CASE 9

void sortiraj(struct student s[20]) {
	//zapisi u vektor i sortiraj
	std::vector<int> bodovi;
	int i, j;
	for (j = 0; j < structSize; j++) {
		bodovi.push_back(s[j].totalscore);
	}
	std::sort(bodovi.begin(), bodovi.end());

	for (j = 0; j < structSize; j++) { //po vektoru
		for (i = 0; i < structSize; i++) { //po strukturama
			if (s[i].totalscore == bodovi[j]) {
				std::cout << "\n--------------------------\n";
				std::cout << ">>>  ID: " << s[i].ID << std::endl;
				std::cout << ">>>  Name: " << s[i].name << std::endl;
				std::cout << ">>>  Sex: " << s[i].sex << std::endl;
				std::cout << ">>>  Test1: " << s[i].test1 << std::endl;
				std::cout << ">>>  Test2: " << s[i].test2 << std::endl;
				std::cout << ">>>  Mid-term score: " << s[i].midterm << std::endl;
				std::cout << ">>>  Final score: " << s[i].finscore << std::endl;
				std::cout << ">>>  Total score: " << s[i].totalscore << std::endl;
				std::cout << "--------------------------\n\n";
			}
		}
		i = 0;
	}

}


//---------IZBORNIK---------

void izbornik(struct student s[20]) {

	int odabir;
	do {
		std::cout << "\n\n***DOBRODOSLI U IZBORNIK***\n\n";
		std::cout << ">>>  1  - Dodaj novi zapis" << std::endl;
		std::cout << ">>>  2  - Ukloni zapis" << std::endl;
		std::cout << ">>>  3  - Azuriraj zapis" << std::endl;
		std::cout << ">>>  4  - Prikazi sve zapise" << std::endl;
		std::cout << ">>>  5  - Izracunaj prosjek bodova za studenta" << std::endl;
		std::cout << ">>>  6  - Prikazi studenta s najvecim brojem bodova" << std::endl;
		std::cout << ">>>  7  - Prikazi studenta s najmanjim brojem bodova" << std::endl;
		std::cout << ">>>  8  - Pronadi studenta po ID-u" << std::endl;
		std::cout << ">>>  9  - Sortiraj zapise po broju bodova (total)" << std::endl;
		std::cout << ">>>  0  - Izlaz\n" << std::endl;
		std::cout << "UNESITE ODABIR: ";
		std::cin >> odabir;
		std::cout << std::endl;
		std::cout << std::endl;

		switch (odabir){

		case 1:
			dodaj_novi_zapis(s);
			return izbornik(s);
		case 2:
			ukloni_zapis(s);
			return izbornik(s);
		case 3:
			azuriraj_zapis(s);
			break;
		case 4:
			prikazi_zapis(s);
			return izbornik(s);
		case 5:
			prosjek_bodova(s);
			return izbornik(s);
		case 6:
			najvise_bodova(s);
			return izbornik(s);
		case 7:
			najmanje_bodova(s);
			return izbornik(s);
		case 8:
			pretraga(s);
			return izbornik(s);
		case 9:
			sortiraj(s);
			break;
		case 0:
			std::cout << "\nOdabrali ste izlaz.\nDovidjenja!!!\n\n_________________________________________\n\n";
			break;
		default: 
			std::cout << "\nUnijeli ste krivi broj, pokusajte ponovo!\n\n_________________________________________\n\n";
			break;
		}
	} while (odabir < 0 || odabir > 10);
}


int main() {
	
	struct student s[20] = {{"1001", "John", "M", 5, 5, 5, 4, 19},
							{"1002", "Ronald", "M", 5, 3, 5, 5, 18},
							{"1003", "Reuel", "M", 5, 3, 5, 2, 15},
							{"1004", "Tolkien", "M", 5, 3, 5, 3, 16},

	};
	
	izbornik(s);

}