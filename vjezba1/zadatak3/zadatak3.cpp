#include "pch.h"
#include <iostream>

/*
U nizu cetveroznamenkastih brojeva nalazi se samo jedan broj cija je suma jedinice i
stotice jednaka 5. Napisite funkciju koja pronalazi taj broj u nizu od N brojeva i vraca
referencu na taj element niza. Koristeci povratnu vrijednost funkcije kao lvalue uvecajte
taj element niza za jedan. Ispisite vrijednost elementa poslije uvecavanja.
*/

int numb(int *arr, int len) {
	int i = 0;
	for (i; i < len; i++) {
		if (arr[i] % 10 + (arr[i] % 1000) / 100 == 5) {
			int &r = arr[i];
			return r;
		}
	}
}

int main() {

	int arr[] = { 1234,2345,3456,2174,6789 };
	int len = sizeof(arr) / sizeof(*arr);
	std::cout << "Uvecani broj: " << numb(arr, len) + 1 << std::endl;
}
